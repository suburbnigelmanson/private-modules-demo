# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.2.0"></a>
# [1.2.0](https://bitbucket.org/benjamincoe/private-modules-demo/compare/v1.1.0...v1.2.0) (2016-05-06)


### Bug Fixes

* don't print out .npmrc now that we've perfected things ([51243e2](https://bitbucket.org/benjamincoe/private-modules-demo/commits/51243e2))

### Features

* refactored script to no longer require both NPM_REGISTRY_HOST and NPM_REGISTRY_URL ([549868d](https://bitbucket.org/benjamincoe/private-modules-demo/commits/549868d))



<a name="1.1.0"></a>
# 1.1.0 (2016-04-26)


### Bug Fixes

* \n was not escaped as expected ([11ae31e](https://bitbucket.org/benjamincoe/private-modules-demo/commits/11ae31e))
* file should be named bitbucket-pipelines.yml ([505b344](https://bitbucket.org/benjamincoe/private-modules-demo/commits/505b344))

### Features

* add example private module pipeline ([1350487](https://bitbucket.org/benjamincoe/private-modules-demo/commits/1350487))
