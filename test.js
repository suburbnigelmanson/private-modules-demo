/* global describe, it */

var privateModulesDemo = require('./')
require('chai').should()

describe('private-modules-demo', function () {
  it('it can install and run a private npm module', function () {
    privateModulesDemo().should.equal('bcoe is groovy')
  })
})
